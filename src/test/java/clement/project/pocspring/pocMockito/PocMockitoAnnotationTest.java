package clement.project.pocspring.pocMockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PocMockitoAnnotationTest {

    @Mock
    DataClass mockedDataClass;

    @InjectMocks
    BusinessClass businessClass;


    @Test
    public void testFindMax(){
        when(mockedDataClass.getData()).thenReturn(new int[]{12, 30, 99});
        assertEquals(99, businessClass.findMax());
    }

    @Test
    public void testFindMaxDataEmpty(){
        when(mockedDataClass.getData()).thenReturn(new int[]{});
        assertNull(businessClass.findMax());
    }

}
