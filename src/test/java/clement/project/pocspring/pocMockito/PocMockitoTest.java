package clement.project.pocspring.pocMockito;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PocMockitoTest {

    private DataClass mockedDataClass = mock(DataClass.class);
    private BusinessClass businessClass = new BusinessClass(mockedDataClass);

    @Test
    public void testFindMax(){
        when(mockedDataClass.getData()).thenReturn(new int[]{12, 30, 99});
        assertEquals(99, businessClass.findMax());
    }

    @Test
    public void testFindMaxDataEmpty(){
        when(mockedDataClass.getData()).thenReturn(new int[]{});
        assertNull(businessClass.findMax());
    }

    /**
     * first time getData() will return [12, 30, 99], and next time (all other the next time) will return [12, 30, 9]
     */
    @Test
    public void testMultipleReturn(){
        when(mockedDataClass.getData()).thenReturn(new int[]{12, 30, 99}).thenReturn(new int[]{12, 30, 9});
        assertEquals(99, businessClass.findMax());
        assertEquals(30, businessClass.findMax());
        assertEquals(30, businessClass.findMax());
    }

    /**
     * Will only return 3 for param 0, other will return null (because the list contains nothing)
     */
    @Test
    public void testSpecificParam(){
        List list = mock(List.class);
        when(list.get(0)).thenReturn(3);
        assertEquals(3, list.get(0));
        assertNull(list.get(1));
    }

    /**
     * Will return 3 for every param which is type Int
     */
    @Test
    public void testGenericParam(){
        List list = mock(List.class);
        when(list.get(Mockito.anyInt())).thenReturn(3);
        assertEquals(3, list.get(0));
        assertEquals(3, list.get(1));
        assertEquals(3, list.get(999));
    }



}
