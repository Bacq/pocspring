package clement.project.pocspring.pocSpringTest;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;


/*
    With Spring boot we can replace these 2 lines by :
    @SpringBootTest
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PocSpringTest.class)
public class PocSpringTestTest {

    @Autowired
    DependentClass dependentClass;

    @Test
    public void dependentClassInjectionTest(){
        assertEquals(5, dependentClass.getMax());
    }



}
