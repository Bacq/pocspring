package clement.project.pocspring.pocMockitoSpring;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.CollectionUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

/**
 * Mock the injected bean instead of using a Spring context (much faster)
 */

@ExtendWith(MockitoExtension.class)
public class PocMockitoSpringTest {

    @InjectMocks
    DependentClass dependentClass;

    @Mock
    FakeRepo fakeRepo;

    @Test
    public void dependentClassInjectionTest(){
        when(fakeRepo.findAll()).thenReturn((List<Integer>) CollectionUtils.arrayToList(new int[]{1, 12, 9, 105}));
        assertEquals(105, dependentClass.getMax());
    }

    @Test
    public void testGetMaxNoValues(){
        when(fakeRepo.findAll()).thenReturn((List<Integer>) CollectionUtils.arrayToList(new int[]{}));
        assertNull(dependentClass.getMax());
    }


    @Test
    public void testGetMaxMultipleMax(){
        when(fakeRepo.findAll()).thenReturn((List<Integer>) CollectionUtils.arrayToList(new int[]{1, 105, 9, 105}));
        assertEquals(105, dependentClass.getMax());
    }


}
