package clement.project.pocspring.pocSpringAopCustomAnnotation;



import clement.project.pocspring.pocSpringAopCustomAnnotation.business.BusinessService1;
import clement.project.pocspring.pocSpringAopCustomAnnotation.business.BusinessService2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocSpringAopCustomAnnotation implements CommandLineRunner {

    private static Logger LOGGER = LoggerFactory.getLogger(PocSpringAopCustomAnnotation.class);

    private BusinessService1 businessService1;
    private BusinessService2 businessService2;

    public PocSpringAopCustomAnnotation(BusinessService1 businessService1, BusinessService2 businessService2) {
        this.businessService1 = businessService1;
        this.businessService2 = businessService2;
    }

    public static void main(String[] args) {
        SpringApplication.run(PocSpringAopCustomAnnotation.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println();
        LOGGER.info("businessService1 : {}", businessService1.doSomething());
        System.out.println();
        LOGGER.info("businessService2 : {}", businessService2.doSomething());
    }
}
