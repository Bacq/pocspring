package clement.project.pocspring.pocSpringAopCustomAnnotation.business;

import clement.project.pocspring.pocSpringAopCustomAnnotation.data.Dao2;
import org.springframework.stereotype.Service;

@Service
public class BusinessService2 {

    private Dao2 dao2;

    public BusinessService2(Dao2 dao2) {
        this.dao2 = dao2;
    }

    public String doSomething(){
        return dao2.getData();
    }
}
