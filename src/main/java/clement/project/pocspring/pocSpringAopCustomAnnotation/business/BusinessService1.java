package clement.project.pocspring.pocSpringAopCustomAnnotation.business;

import clement.project.pocspring.pocSpringAopCustomAnnotation.aspect.TrackTime;
import clement.project.pocspring.pocSpringAopCustomAnnotation.data.Dao1;
import org.springframework.stereotype.Service;

@Service
public class BusinessService1 {

    private Dao1 dao1;


    public BusinessService1(Dao1 dao1) {
        this.dao1 = dao1;
    }

    @TrackTime
    public String doSomething(){
        return dao1.getData();
    }
}
