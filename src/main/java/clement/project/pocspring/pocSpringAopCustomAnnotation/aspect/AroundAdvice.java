package clement.project.pocspring.pocSpringAopCustomAnnotation.aspect;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class AroundAdvice {

    private static Logger LOGGER = LoggerFactory.getLogger(AroundAdvice.class);

    @Around("clement.project.pocspring.pocSpringAopCustomAnnotation.aspect.CommonPointCutConfig.getTrackTimePointCut()")
    public Object trackTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        //Important to return the result, or the method will return null
        Object result = joinPoint.proceed();
        long timeTaken = System.currentTimeMillis() - startTime;
        LOGGER.info("Method {} take {} milliSecond to bean(lol) executed", joinPoint.getSignature(), timeTaken);
        return result;
    }

}
