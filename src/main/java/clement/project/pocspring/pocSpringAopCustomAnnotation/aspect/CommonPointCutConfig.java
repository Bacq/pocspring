package clement.project.pocspring.pocSpringAopCustomAnnotation.aspect;

import org.aspectj.lang.annotation.Pointcut;

public class CommonPointCutConfig {

    @Pointcut("@annotation(clement.project.pocspring.pocSpringAopCustomAnnotation.aspect.TrackTime)")
    public void getTrackTimePointCut(){}

}
