package clement.project.pocspring.pocSpringAopCustomAnnotation.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//Annotation only for methods
@Target(ElementType.METHOD)
//Annotation available at runtime
@Retention(RetentionPolicy.RUNTIME)
public @interface TrackTime {

}
