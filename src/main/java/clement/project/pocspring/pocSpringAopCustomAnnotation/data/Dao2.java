package clement.project.pocspring.pocSpringAopCustomAnnotation.data;

import org.springframework.stereotype.Repository;

@Repository
public class Dao2 {

    public String getData(){
        return "Dao2";
    }
}
