package clement.project.pocspring.pocGenericService.dao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class RandomEntity implements GenericEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String label;

    public RandomEntity() {
    }

    public RandomEntity(String label) {
        this.label = label;
    }

    @Override
    public Long getId() {
        return id;
    }

}
