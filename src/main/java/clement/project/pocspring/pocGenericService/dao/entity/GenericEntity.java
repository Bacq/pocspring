package clement.project.pocspring.pocGenericService.dao.entity;

public interface GenericEntity<I> {

    public I getId();
}
