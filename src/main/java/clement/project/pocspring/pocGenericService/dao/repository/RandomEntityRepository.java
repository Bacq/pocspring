package clement.project.pocspring.pocGenericService.dao.repository;

import clement.project.pocspring.pocGenericService.dao.entity.RandomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RandomEntityRepository extends JpaRepository<RandomEntity, Long> {
}
