package clement.project.pocspring.pocGenericService;


import clement.project.pocspring.pocGenericService.dao.entity.RandomEntity;
import clement.project.pocspring.pocGenericService.service.GenericDefaultService;
import clement.project.pocspring.pocGenericService.service.GenericService;
import clement.project.pocspring.pocGenericService.service.impl.GenericDefaultServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@Slf4j
public class PocSpringGenericService implements CommandLineRunner {

    private static Logger LOGGER = LoggerFactory.getLogger(PocSpringGenericService.class);


    public PocSpringGenericService() {

    }

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(PocSpringGenericService.class, args);
        GenericService genericService = applicationContext.getBean(GenericService.class);
        GenericDefaultService genericDefaultService = applicationContext.getBean(GenericDefaultService.class);


        genericService.create(new RandomEntity("from genericService"));
        genericDefaultService.create(new RandomEntity("from genericDefaultService"));

        log.info("all entity created (1 from genericService & 1 from genericDefaultService) : {}", genericService.findAll());
        log.info("all entity created (1 from genericService & 1 from genericDefaultService) : {}", genericDefaultService.findAll());
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
