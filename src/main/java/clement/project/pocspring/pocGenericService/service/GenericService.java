package clement.project.pocspring.pocGenericService.service;

import java.util.List;

public interface GenericService <M, I> {

    public M create(M modelToCreate);

    public M findById(I id);

    public List<M> findAll();

    public M update(M objectToUpdate);

    public void delete(M objectToDelete);

}
