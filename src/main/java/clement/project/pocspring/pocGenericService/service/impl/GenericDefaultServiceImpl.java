package clement.project.pocspring.pocGenericService.service.impl;

import clement.project.pocspring.pocGenericService.dao.entity.RandomEntity;
import clement.project.pocspring.pocGenericService.dao.repository.RandomEntityRepository;
import clement.project.pocspring.pocGenericService.service.GenericDefaultService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class GenericDefaultServiceImpl implements GenericDefaultService<RandomEntity, Long> {

    private RandomEntityRepository randomEntityRepository;

    public GenericDefaultServiceImpl(RandomEntityRepository randomEntityRepository) {
        this.randomEntityRepository = randomEntityRepository;
    }

    public void test(){
        create(new RandomEntity("Test"));
    }

    @Override
    public JpaRepository<RandomEntity, Long> getRepository() {
        return randomEntityRepository;
    }
}
