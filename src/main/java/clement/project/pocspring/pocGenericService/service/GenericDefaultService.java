package clement.project.pocspring.pocGenericService.service;


import clement.project.pocspring.pocGenericService.dao.entity.GenericEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Generic sercvice to implements CRUD
 * @param <M> data model
 * @param <I> dat Identifier (id)
 */
public interface GenericDefaultService<M extends GenericEntity<I>, I> {

    public default M create(M modelToCreate){
        if(modelToCreate.getId() != null && findById(modelToCreate.getId()) == null){
            return null;
        }
        return getRepository().save(modelToCreate);
    }

    public default M findById(I id){
        return getRepository().findById(id).orElse(null);
    }

    public default List<M> findAll(){
        return getRepository().findAll();
    }

    public default M update(M objectToUpdate){
        return getRepository().save(objectToUpdate);
    }

    public default void delete(M objectToDelete){
        getRepository().delete(objectToDelete);
    }

    JpaRepository<M, I> getRepository();

}
