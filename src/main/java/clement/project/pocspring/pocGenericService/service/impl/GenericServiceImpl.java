package clement.project.pocspring.pocGenericService.service.impl;

import clement.project.pocspring.pocGenericService.dao.entity.RandomEntity;
import clement.project.pocspring.pocGenericService.dao.repository.RandomEntityRepository;
import clement.project.pocspring.pocGenericService.service.GenericService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenericServiceImpl implements GenericService<RandomEntity, Long> {

    private RandomEntityRepository randomEntityRepository;

    public GenericServiceImpl(RandomEntityRepository randomEntityRepository) {
        this.randomEntityRepository = randomEntityRepository;
    }

    @Override
    public RandomEntity create(RandomEntity modelToCreate) {
        if(modelToCreate.getId() != null && findById(modelToCreate.getId()) == null){
            return null;
        }
        return randomEntityRepository.save(modelToCreate);
    }

    @Override
    public RandomEntity findById(Long id) {
        return randomEntityRepository.findById(id).orElse(null);
    }

    @Override
    public List<RandomEntity> findAll() {
        return randomEntityRepository.findAll();
    }

    @Override
    public RandomEntity update(RandomEntity objectToUpdate) {
        return randomEntityRepository.save(objectToUpdate);
    }

    @Override
    public void delete(RandomEntity objectToDelete) {
        randomEntityRepository.delete(objectToDelete);
    }
}
