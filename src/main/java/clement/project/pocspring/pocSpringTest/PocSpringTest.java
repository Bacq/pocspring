package clement.project.pocspring.pocSpringTest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PocSpringTest {

    private static Logger LOGGER = LoggerFactory.getLogger(PocSpringTest.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PocSpringTest.class, args);
        DependentClass dependentClass = applicationContext.getBean(DependentClass.class);

        LOGGER.info("date result : {}", dependentClass.getMax());

    }

}
