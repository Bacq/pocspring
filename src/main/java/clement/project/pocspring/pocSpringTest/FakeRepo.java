package clement.project.pocspring.pocSpringTest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FakeRepo  {

    private static Logger LOGGER = LoggerFactory.getLogger(FakeRepo.class);

    private List<Integer> data;

    public FakeRepo() {
        data = new ArrayList<>();
        data.add(0);
        data.add(1);
        data.add(2);
        data.add(3);
        data.add(4);
        data.add(5);
    }

    public List<Integer> findAll(){
        return data;
    }

}
