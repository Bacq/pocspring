package clement.project.pocspring.pocBeanLifeCycle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class RandomBean {

    private static Logger LOGGER = LoggerFactory.getLogger(RandomBean.class);

    public String methodTodo(){
        return "Rush b !";
    }

    @PostConstruct
    public void postConstruct(){
        LOGGER.info("@PostConstruct of bean RandomBean");
    }

    /**
     * Launched at the destruction of the bean (don't know why, but when the application is closed)
     */
    @PreDestroy
    public void preDestroy(){
        LOGGER.info("@PreDestroy of bean RandomBean");
    }
    
}
