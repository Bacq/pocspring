package clement.project.pocspring.pocBeanLifeCycle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PocBeanLifeCycle {

    private static Logger LOGGER = LoggerFactory.getLogger(PocBeanLifeCycle.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PocBeanLifeCycle.class, args);

        RandomBean randomBean = applicationContext.getBean(RandomBean.class);
        LOGGER.info("randomBean result : {}", randomBean.methodTodo());

    }
}
