package clement.project.pocspring.pocMockito;

import java.util.Arrays;
import java.util.stream.IntStream;

public class BusinessClass {

    private DataClass dataClass;

    public BusinessClass(DataClass dataClass) {
        this.dataClass = dataClass;
    }

    /**
     *
     * @return the max value, if the tab is empty, return 0
     */
    public Integer findMax(){
        int[] data = this.dataClass.getData();
        return (data.length > 0)?
                Arrays.stream(data).summaryStatistics().getMax() :
                null;
    }


}
