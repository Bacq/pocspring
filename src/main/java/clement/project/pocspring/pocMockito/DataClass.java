package clement.project.pocspring.pocMockito;

public class DataClass {

    private int[] data;

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }
}
