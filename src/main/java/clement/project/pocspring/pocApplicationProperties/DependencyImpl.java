package clement.project.pocspring.pocApplicationProperties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DependencyImpl implements DependencyInterface {

    @Value("${server.port}")
    private int port;

    @Override
    public String methodToImplement() {
        return ""+port;
    }


}
