package clement.project.pocspring.pocApplicationProperties;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PocApplicationProperties {

    private static Logger LOGGER = LoggerFactory.getLogger(PocApplicationProperties.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PocApplicationProperties.class, args);

        DependentClass dependentClass = applicationContext.getBean(DependentClass.class);
        LOGGER.info("dependentClass.toDoMethod() result : {}", dependentClass.toDoMethod());

    }

}

/**
 * edit run configuration to set profile dev to use application-dev.yaml
 */
