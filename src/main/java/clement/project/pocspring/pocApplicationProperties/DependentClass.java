package clement.project.pocspring.pocApplicationProperties;

import org.springframework.stereotype.Service;

@Service
public class DependentClass {

    private DependencyInterface dependencyInterface;

    public DependentClass(DependencyInterface dependencyInterface) {
        this.dependencyInterface = dependencyInterface;
    }

    public String toDoMethod(){
        return this.dependencyInterface.methodToImplement();
    }




}
