package clement.project.pocspring.pocComponentScan.firstPackage;

import clement.project.pocspring.pocComponentScan.secondPackage.RandomBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
/**
 * Redifine @ComponentScan to add the secondPackage to be scanned
 */
@ComponentScan({"clement.project.pocspring.pocComponentScan.secondPackage", "clement.project.pocspring.pocComponentScan.firstPackage"})
public class PocComponentScan {

    private static Logger LOGGER = LoggerFactory.getLogger(PocComponentScan.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PocComponentScan.class, args);

        RandomBean randomBean = applicationContext.getBean(RandomBean.class);
        LOGGER.info("randomBean method result : {}", randomBean.methodToImplement());

        RandomBean2 randomBean2 = applicationContext.getBean(RandomBean2.class);
        LOGGER.info("randomBean2 method result : {}", randomBean2.methodToImplement());

    }

}