package clement.project.pocspring.pocComponentScan.secondPackage;

import org.springframework.stereotype.Component;

@Component
public class RandomBean {

    public String methodToImplement() {
        return "Hello friend !";
    }
}
