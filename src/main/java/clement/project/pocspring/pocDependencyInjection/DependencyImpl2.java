package clement.project.pocspring.pocDependencyInjection;

import org.springframework.stereotype.Component;

@Component
public class DependencyImpl2 implements DependencyInterface {

    @Override
    public String methodToImplement() {
        return "DependencyImpl2";
    }
}
