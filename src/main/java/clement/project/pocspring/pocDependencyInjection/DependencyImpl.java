package clement.project.pocspring.pocDependencyInjection;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class DependencyImpl implements DependencyInterface {


    @Override
    public String methodToImplement() {
        return "DependencyImpl";
    }
}
