package clement.project.pocspring.pocDependencyInjection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DependentClass {

    private DependencyInterface DependencyInterface;

    /**
     * Dependency Injection by Contructor
     * There is 2 bean that implement DependencyInterface, it will use the one with primary (DependencyImpl)
     * @param DependancyInterface
     */
    public DependentClass(DependencyInterface DependancyInterface) {
        this.DependencyInterface = DependancyInterface;
    }

//    /**
//     * Dependency Injection by Contructor
//     * There is 2 bean that implement DependencyInterface, it will use the one specified in the qualifier (DependencyImpl2) overt the one with primary (DependencyImpl)
//     * @param DependancyInterface
//     */
//    public DependentClass(@Qualifier("dependencyImpl2") DependencyInterface DependencyInterface) {
//        this.DependencyInterface = DependencyInterface;
//    }

    public String toDoMethod(){
        return this.DependencyInterface.methodToImplement();
    }




}
