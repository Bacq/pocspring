package clement.project.pocspring.pocDependencyInjection;

import clement.project.pocspring.pocScope.PocBeanScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PocDependencyInjection {

    private static Logger LOGGER = LoggerFactory.getLogger(PocBeanScope.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PocDependencyInjection.class, args);

        DependentClass dependantClass = applicationContext.getBean(DependentClass.class);
        LOGGER.info("Implentation used : {}", dependantClass.toDoMethod());


    }

}
