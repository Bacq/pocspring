package clement.project.pocspring.pocSpringAOP.business;

import clement.project.pocspring.pocSpringAOP.data.Dao1;
import org.springframework.stereotype.Service;

@Service
public class BusinessService1 {

    private Dao1 dao1;

    public BusinessService1(Dao1 dao1) {
        this.dao1 = dao1;
    }

    public String doSomething(){
        return dao1.getData();
    }
}
