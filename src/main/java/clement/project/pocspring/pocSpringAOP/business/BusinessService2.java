package clement.project.pocspring.pocSpringAOP.business;

import clement.project.pocspring.pocSpringAOP.data.Dao2;
import org.springframework.stereotype.Service;

@Service
public class BusinessService2 {

    private Dao2 dao2;

    public BusinessService2(Dao2 dao2) {
        this.dao2 = dao2;
    }

    public String doSomething(){
        return dao2.getData();
    }
}
