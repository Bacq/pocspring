package clement.project.pocspring.pocSpringAOP;


import clement.project.pocspring.pocSpringAOP.business.BusinessService1;
import clement.project.pocspring.pocSpringAOP.business.BusinessService2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PocSpringAOP implements CommandLineRunner {

    private static Logger LOGGER = LoggerFactory.getLogger(PocSpringAOP.class);

    private BusinessService1 businessService1;
    private BusinessService2 businessService2;

    public PocSpringAOP(BusinessService1 businessService1, BusinessService2 businessService2) {
        this.businessService1 = businessService1;
        this.businessService2 = businessService2;
    }

    public static void main(String[] args) {
        SpringApplication.run(PocSpringAOP.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println();
        LOGGER.info("businessService1 : {}", businessService1.doSomething());
        System.out.println();
        LOGGER.info("businessService2 : {}", businessService2.doSomething());
    }
}
