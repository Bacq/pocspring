package clement.project.pocspring.pocSpringAOP.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class AfterAspect {

    private static Logger LOGGER = LoggerFactory.getLogger(AfterAspect.class);

    //After execution successful
    @AfterReturning(value = "execution(* clement.project.pocspring.pocSpringAOP.business.*.*(..))",
            returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result){
        //Advice
        LOGGER.info("After completion of methode {}, return value {}", joinPoint.getSignature(), (String)result);
    }

    //@AfterThrowing => After throwing exception
    //@After => after, whatever it is successful or throw an exception

}
