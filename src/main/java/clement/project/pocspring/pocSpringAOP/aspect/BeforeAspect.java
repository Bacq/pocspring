package clement.project.pocspring.pocSpringAOP.aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect //Aspect = pointcut + advice
@Configuration
public class BeforeAspect {

    private static Logger LOGGER = LoggerFactory.getLogger(BeforeAspect.class);

    //pointcut =>   execution(<TARGET_RETURN_TYPE> <PACKAGE>.<TARGET_CLASS>.<TARGET-METHOD(S)(<TARGET_ARGUMENT(S)_TYPE>))

    @Before("execution(* clement.project.pocspring.pocSpringAOP.business.*.*(..))")
    public void beforePackageBusiness(JoinPoint joinPoint){
        //Advice
        LOGGER.info("Before methods in package business : {}", joinPoint);
    }


    //Using external config PointCut
    @Before("clement.project.pocspring.pocSpringAOP.aspect.CommonPointCutConfig.getBusinessPointCut()")
    public void beforePackageBusinessUsingGetPointCut(JoinPoint joinPoint){
        //Advice
        LOGGER.info("Before methods in package business using method to get PointCut : {}", joinPoint.getSignature());
    }

    //Using external config PointCut (every bean that begin with "Dao")
    @Before("clement.project.pocspring.pocSpringAOP.aspect.CommonPointCutConfig.getBeanStartingWithDao()")
    public void beforePackageBusinessUsingGetBean(JoinPoint joinPoint){
        //Advice
        LOGGER.info("Before bean that begin name begin with \"dao\" : {}", joinPoint.getSignature());
    }

//    @Before("execution(* clement.project.pocspring.pocSpringAOP..*.*(..))")
//    public void beforePackagePocSpringAOP(JoinPoint joinPoint){
//        //Advice
//        LOGGER.info("Before methods in package pocSpringAOP : {}", joinPoint);
//    }

//    @Before("execution(* clement.project.pocspring.pocSpringAOP.business.BusinessService2.*(..))")
//    public void beforeBusinessService2(JoinPoint joinPoint){
//        //Advice
//        LOGGER.info("Before methods in class BusinessService2 business : {}", joinPoint);
//    }

}
