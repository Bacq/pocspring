package clement.project.pocspring.pocSpringAOP.aspect;

import org.aspectj.lang.annotation.Pointcut;

public class CommonPointCutConfig {

    @Pointcut("execution(* clement.project.pocspring.pocSpringAOP.business.*.*(..))")
    public void getBusinessPointCut(){
    }

    @Pointcut("execution(* clement.project.pocspring.pocSpringAOP.business.*.*(..))")
    public void getDataPointCut(){
    }

    @Pointcut("getBusinessPointCut() && getDataPointCut()")
    public void getPocSpringAopPointCut(){
    }

    @Pointcut("bean(dao*)")
    public void getBeanStartingWithDao(){
    }
}
