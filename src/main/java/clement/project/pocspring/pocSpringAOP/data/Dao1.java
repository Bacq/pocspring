package clement.project.pocspring.pocSpringAOP.data;

import org.springframework.stereotype.Repository;

@Repository
public class Dao1 {

    public String getData(){
        return "Dao1";
    }
}
