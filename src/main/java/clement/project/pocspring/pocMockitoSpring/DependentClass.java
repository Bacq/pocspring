package clement.project.pocspring.pocMockitoSpring;

import org.springframework.stereotype.Service;

@Service
public class DependentClass {

    private FakeRepo fakeRepo;

    public DependentClass(FakeRepo fakeRepo) {
        this.fakeRepo = fakeRepo;
    }

    public Integer getMax(){
        return fakeRepo.findAll().stream()
                .reduce((accu, current) -> {
                    return (current > accu)? current : accu;
                }).orElse(null);
    }

}
