package clement.project.pocspring.pocSpringJPA;


import clement.project.pocspring.pocSpringJPA.model.Book;
import clement.project.pocspring.pocSpringJPA.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class PocSpringJPA{

    private static Logger LOGGER = LoggerFactory.getLogger(PocSpringJPA.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(PocSpringJPA.class, args);
        BookRepository bookRepository = applicationContext.getBean(BookRepository.class);
        bookRepository.save(new Book("LOTR", "Tolkien"));
        bookRepository.save(new Book("LOTR_2", "Tolkien"));
        bookRepository.save(new Book("LOTR_3", "Tolkien"));
    }

}
