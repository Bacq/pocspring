package clement.project.pocspring.pocSpringJPA.service;

import clement.project.pocspring.pocSpringJPA.model.Book;

import java.util.List;

public interface BookService {

    public List<Book> getAll();
    public Book create(Book bookToCreate);

}
