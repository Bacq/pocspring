package clement.project.pocspring.pocSpringJPA.service;

import clement.project.pocspring.pocSpringJPA.model.Book;
import clement.project.pocspring.pocSpringJPA.repository.BookRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@Primary
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

//    private List<Book> books = Arrays.asList(new Book(1, "Lord Of The Rings", "J.R.R. Tolkien"));

    @Override
    public List<Book> getAll(){
        return bookRepository.findAll();
    }

    @Override
    public Book create(Book bookToCreate) {
        if(bookRepository.findById(bookToCreate.getId()).isPresent()){
            return null;
        }
        return bookRepository.save(bookToCreate);
    }
}
