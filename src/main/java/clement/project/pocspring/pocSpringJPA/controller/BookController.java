package clement.project.pocspring.pocSpringJPA.controller;

import clement.project.pocspring.pocSpringJPA.model.Book;
import clement.project.pocspring.pocSpringJPA.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class BookController {

    private static Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/books")
    public ResponseEntity<List<Book>> getAllBooks() {
        List<Book> books = bookService.getAll();
        if(CollectionUtils.isEmpty(books)){
            LOGGER.warn("getAllBooks => bookService.getAll() return nothing or is null");
            return ResponseEntity.notFound().build();
        } else {
            LOGGER.info("getAllBooks => bookService.getAll() return some books (id={})",
                    books.stream()
                    .map(book -> book.getId())
                    .collect(Collectors.toList()));
            return ResponseEntity.ok().body(books);
        }
    }

    @PostMapping("/book")
    public ResponseEntity<Book> createBook(@RequestBody Book bookToCreate) {
        Book bookCreated = bookService.create(bookToCreate);
        if(bookCreated == null){
            LOGGER.warn("createBook => bookService.create() return nothing or is null");
            return ResponseEntity.badRequest().build();
        } else {
            LOGGER.info("createBook => bookService.create() return a book (id={})", bookCreated.getId());
            return ResponseEntity.ok().body(bookCreated);
        }
    }




}
