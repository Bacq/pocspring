package clement.project.pocspring.pocSpringJPA.repository;


import clement.project.pocspring.pocSpringJPA.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
}
