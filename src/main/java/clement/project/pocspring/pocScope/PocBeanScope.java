package clement.project.pocspring.pocScope;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PocBeanScope {

    private static Logger LOGGER = LoggerFactory.getLogger(PocBeanScope.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PocBeanScope.class, args);

        System.out.println();
        LOGGER.info("Default scope bean (singleton) :");
        BeanByDefaultSingleton defaultBean = applicationContext.getBean(BeanByDefaultSingleton.class);
        BeanByDefaultSingleton defaultBean2 = applicationContext.getBean(BeanByDefaultSingleton.class);
        //Modify only the first bean of BeanByDefaultSingleton
        defaultBean.modifyValueInBean();
        LOGGER.info("defaultBean.modifyValueInBean()");
        LOGGER.info("Value of defaultBean : {}", defaultBean.methodToImplement());
        LOGGER.info("Value of defaultBean2 : {}", defaultBean2.methodToImplement());

        System.out.println();
        LOGGER.info("Prototype scope bean :");
        BeanScopePrototype prototypeBean = applicationContext.getBean(BeanScopePrototype.class);
        BeanScopePrototype prototypeBean2 = applicationContext.getBean(BeanScopePrototype.class);
        //Modify only the first bean of BeanScopePrototype
        prototypeBean.modifyValueInBean();
        LOGGER.info("prototypeBean.modifyValueInBean()");
        LOGGER.info("Value of prototypeBean : {}", prototypeBean.methodToImplement());
        LOGGER.info("Value of prototypeBean2 : {}", prototypeBean2.methodToImplement());
    }

}
