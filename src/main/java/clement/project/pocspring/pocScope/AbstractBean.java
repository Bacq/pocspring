package clement.project.pocspring.pocScope;

public class AbstractBean {
    private String toString = "Unmodified";

    public String methodToImplement() {
        return this.toString;
    }

    public void modifyValueInBean() {
        this.toString = "Modified !";
    }

}
