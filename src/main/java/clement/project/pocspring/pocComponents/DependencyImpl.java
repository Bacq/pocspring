package clement.project.pocspring.pocComponents;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class DependencyImpl implements DependencyInterface {


    @Override
    public String methodToImplement() {
        return "DependencyImpl";
    }
}
