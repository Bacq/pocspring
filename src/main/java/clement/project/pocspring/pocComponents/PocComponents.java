package clement.project.pocspring.pocComponents;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PocComponents {

    private static Logger LOGGER = LoggerFactory.getLogger(PocComponents.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PocComponents.class, args);

        DependentClass dependentClass = applicationContext.getBean(DependentClass.class);
        LOGGER.info("dependentClass.toDoMethod() result : {}", dependentClass.toDoMethod());

    }

}
