package clement.project.pocspring.pocSimpleRestController;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImplGetAllNull implements BookService{
    @Override
    public List<Book> getAll() {
        return null;
    }
}
