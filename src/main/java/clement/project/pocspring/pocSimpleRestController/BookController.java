package clement.project.pocspring.pocSimpleRestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookController {

    private static Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    private BookService bookService;
    private BookService bookServiceGetAllNull;

    public BookController(BookService bookService, @Qualifier("bookServiceImplGetAllNull") BookService bookServiceGetAllNull) {
        this.bookService = bookService;
        this.bookServiceGetAllNull = bookServiceGetAllNull;
    }

    @GetMapping("/books")
    public List<Book> getAllBook(){
        return bookService.getAll();
    }

    @GetMapping("/responseBody")
    public ResponseEntity<List<Book>> getAllBookResponseBody() {
        List<Book> books = bookService.getAll();
        if(CollectionUtils.isEmpty(books)){
            LOGGER.warn("getAllBookResponseBody => bookService.getAll() return nothing or is null");
            return ResponseEntity.notFound().build();
        } else {
            LOGGER.info("getAllBookResponseBody => bookService.getAll() return some books");
            return ResponseEntity.ok().body(books);
        }
    }


    @GetMapping("/responseBodyNotFound")
    public ResponseEntity<List<Book>> getAllBookResponseBodyNotFound() {
        List<Book> books = bookServiceGetAllNull.getAll();
        if(CollectionUtils.isEmpty(books)){
            LOGGER.warn("getAllBookResponseBodyNotFound => bookServiceGetAllNull.getAll() return nothing or is null");
            return ResponseEntity.notFound().build();
        } else {
            LOGGER.info("getAllBookResponseBodyNotFound => bookServiceGetAllNull.getAll() return some books");
            return ResponseEntity.ok().body(books);
        }
    }

}
