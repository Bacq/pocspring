package clement.project.pocspring.pocSimpleRestController;

import java.util.List;

public interface BookService {

    public List<Book> getAll();

}
