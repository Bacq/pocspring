package clement.project.pocspring.pocSimpleRestController;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PocSimpleRestController {

    private static Logger LOGGER = LoggerFactory.getLogger(PocSimpleRestController.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PocSimpleRestController.class, args);

    }

}
