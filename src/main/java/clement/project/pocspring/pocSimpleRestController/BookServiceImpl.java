package clement.project.pocspring.pocSimpleRestController;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@Primary
public class BookServiceImpl implements BookService{

    private List<Book> books = Arrays.asList(new Book(1L, "Lord Of The Rings", "J.R.R. Tolkien"));

    @Override
    public List<Book> getAll(){
        return books;
    }
}
